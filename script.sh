#!/bin/bash
nom_fichier="created_by_script.txt"

if [ -e "$nom_fichier" ]; then
    echo "Le fichier existe déjà."
else
   touch "$nom_fichier"
    echo "Fichier créé : $nom_fichier"
fi

echo "Salut ! Comment t'appelles-tu ?"
read nom_utilisateur

echo "Bienvenue, $nom_utilisateur !"
